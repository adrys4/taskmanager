﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TaskManager.Entities;
using TaskManager.Models;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Net.Mail;

namespace TaskManager.Controllers
{
    [Authorize]
    public class IssueController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Issue
        public async Task<ActionResult> Index(string searchTitle, 
                                            string searchPriority, 
                                            string searchStatus, 
                                            string searchDeveloper) {
            var issue = GetFilterResult(searchTitle, searchPriority,searchStatus,searchDeveloper);
            return View(await issue.ToListAsync());
        }

        private IQueryable<Issue> GetFilterResult (string searchTitle,
                                            string searchPriority,
                                            string searchStatus,
                                            string searchDeveloper) {
            var issue = from s in db.Issues select s;
            if (!String.IsNullOrEmpty(searchTitle)) {
                issue = issue.Where(s => s.Title.ToUpper().Contains(searchTitle.ToUpper()));
            }
            if (!String.IsNullOrEmpty(searchPriority)) {
                issue = issue.Where(s => s.PriorityID.ToUpper().Contains(searchPriority.ToUpper()));
            }
            if (!String.IsNullOrEmpty(searchStatus)) {
                issue = issue.Where(s => s.StatusID.ToUpper().Contains(searchStatus.ToUpper()));
            }
            if (!String.IsNullOrEmpty(searchDeveloper)) {
                issue = issue.Where(s => s.UserID.ToUpper().Contains(searchDeveloper.ToUpper()));
            }
            return issue;
        }

        // GET: Issue/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Issue issue = await db.Issues.FindAsync(id);
            if (issue == null)
            {
                return HttpNotFound();
            }
            return View(issue);
        }

        // GET: Issue/Create
        public async Task<ActionResult> Create()
        {
            var model = new Issue
            {
                Priorites = await db.Priorities.ToListAsync(),
                Statuses = await db.Status.ToListAsync(),
                Users = await db.Users.ToListAsync()
            };
            return View(model);
        }

        // POST: Issue/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Title,Description,PriorityID,StatusID,UserID")] Issue issue)
        {
            if (ModelState.IsValid)
            {
                db.Issues.Add(issue);
                await db.SaveChangesAsync();
                await SendMail();
                return RedirectToAction("Index");
            }
            return View(issue);
        }

        private async Task SendMail()
        {
            MailMessage o = new MailMessage("adrianszewiola@outlook.com", "adrianszewiola@outlook.com", "Subject12", "Body12");
            NetworkCredential netCred = new NetworkCredential("adrianszewiola@outlook.com", "Selenium105!");
            SmtpClient smtpobj = new SmtpClient("smtp.live.com", 587);
            smtpobj.EnableSsl = true;
            smtpobj.Credentials = netCred;
            smtpobj.Send(o);
            await Task.Delay(1000);
        }

        // GET: Issue/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Issue issue = await db.Issues.FindAsync(id);
            issue.Priorites = await db.Priorities.ToListAsync();
            issue.Statuses = await db.Status.ToListAsync();
            issue.Users = await db.Users.ToListAsync();

            if (issue == null)
            {
                return HttpNotFound();
            }
            return View(issue);
        }

        // POST: Issue/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,Title,Description,PriorityID,StatusID,UserID")] Issue issue)
        {
            if (ModelState.IsValid)
            {
                db.Entry(issue).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(issue);
        }

        // GET: Issue/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Issue issue = await db.Issues.FindAsync(id);
            if (issue == null)
            {
                return HttpNotFound();
            }
            return View(issue);
        }

        // POST: Issue/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Issue issue = await db.Issues.FindAsync(id);
            db.Issues.Remove(issue);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private IEnumerable<SelectListItem> GetSelectListItems(IEnumerable<string> elements)
        {
            // Create an empty list to hold result of the operation
            var selectList = new List<SelectListItem>();

            // For each string in the 'elements' variable, create a new SelectListItem object
            // that has both its Value and Text properties set to a particular value.
            // This will result in MVC rendering each item as:
            //     <option value="State Name">State Name</option>
            foreach (var element in elements)
            {
                selectList.Add(new SelectListItem
                {
                    Value = element,
                    Text = element
                });
            }

            return selectList;
        }
    }
}
