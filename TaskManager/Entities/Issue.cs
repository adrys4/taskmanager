﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using TaskManager.Models;

namespace TaskManager.Entities
{
    [Table("Issue")]
    public class Issue
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [MaxLength(255)]
        [StringLength(100, ErrorMessage = "{0} musi zawierać co najmniej następującą liczbę znaków: {2}.", MinimumLength = 3)]
        public string Title { get; set; }
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        [DisplayName("Priority")]
        public string PriorityID { get; set; }
        [DisplayName("Status")]
        public string StatusID { get; set; }
        [DisplayName("Developer")]
        public string UserID { get; set; }
        [DisplayName("Priority")]
        public ICollection<Priority> Priorites { get; set; }
        [DisplayName("Status")]
        public ICollection<Status> Statuses { get; set; }
        [DisplayName("Developer")]
        public ICollection<ApplicationUser> Users { get; set; }
    }
}