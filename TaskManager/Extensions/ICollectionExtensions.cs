﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TaskManager.Extensions
{
    public static class ICollectionExtensions
    {
        public static IEnumerable<SelectListItem>
        ToSelectListItem<T>(
        this ICollection<T> items, int selectedValue)
        {
            return from item in items
            select new SelectListItem
            {
                Text = item.GetPropertyValue("Title"),
                Value = item.GetPropertyValue("Id"),
                Selected = item.GetPropertyValue("Id")
                .Equals(selectedValue.ToString())
            };
        }

        public static IEnumerable<SelectListItem>
        ToSelectListItem<T>(
        this ICollection<T> items, string selectedValue)
        {
            return from item in items
                select new SelectListItem
                {
                    Text = item.GetPropertyValue("Title"),
                    Value = item.GetPropertyValue("Title"),
                    Selected = item.GetPropertyValue("Id")
                    .Equals(selectedValue)
                };
        }

        public static IEnumerable<SelectListItem>
        ToSelectListUser<T>(
        this ICollection<T> items, string selectedValue)
        {
            return from item in items
                select new SelectListItem
                {
                    Text = item.GetPropertyValue("UserName"),
                    Value = item.GetPropertyValue("UserName"),
                    Selected = item.GetPropertyValue("Id")
                    .Equals(selectedValue)
                };
        }
    }
}