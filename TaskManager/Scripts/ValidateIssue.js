﻿if ($(':input[id="SaveIssue"]').val() != '') {
    $(':input[id="SaveIssue"]').prop('disabled', false);
}
else {
    $(':input[id="SaveIssue"]').prop('disabled', true);
}
$(document).ready(function () {
    $(':input[id="CreateIssue"]').prop('disabled', true);
	$('input[id="Title"').keyup(function () {
        if ($(this).val() != '') {
            $(':input[id="CreateIssue"]').prop('disabled', false);
        }
        else {
            $(':input[id="CreateIssue"]').prop('disabled', true);
        }
    });
	$('input[id="Title"]').keyup(function () {
        if ($(this).val() != '') {
            $(':input[id="SaveIssue"]').prop('disabled', false);
        }
        else {
            $(':input[id="SaveIssue"]').prop('disabled', true);
        }
    });
});